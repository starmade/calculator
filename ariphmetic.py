def addition(numbers):                      # Функция скалыдвания чисел, аргумент - это массив чисел,
    result = 0                              # чтобы складывать больше одного числа
    for n in numbers:
        result += n
    return result


def subtraction(numbers):                   # Функция вычитания чисел
    index = 1                               # Индекс начального числа в массиве
    result = numbers[0]                     # Число от которого отнимаем другие числа, первое в массиве
    for n in range(len(numbers) - index):   # Делаем цикл в диапозоне на единицу меньше чем длина массива
        result -= numbers[index]            # Отнимаем от первого числа остальные увеличивая индекс
        index += 1
    return result


def multiplication(numbers):
    result = 1
    for n in numbers:
        result *= n
    return result


def division(numbers):
    index = 1
    result = numbers[0]
    for n in range(len(numbers) - index):
        result /= numbers[index]
        index += 1
    return result

