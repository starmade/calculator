from ariphmetic import addition, subtraction, multiplication, division

empty_list = 0
account_numbers = []                                       # Массив чисел над которыми будет проводить операции


def entering():
    while True:
        source_number = input("Enter a numbers through Enter and write <stop> to stop: ")   # Ввод чисел
        if source_number == "stop":                         # Если ввести стоп то цикл остановиться командой break
            break
        try:
            account_numbers.append(float(source_number))    # Через try добавляем в список чисел проверяя численный ввод
        except ValueError:                                  # Обрабатываем ошибку, если ввод не численный
            print("You can calculate only numbers !")


def main_menu():                                         # Логика калькулятора
    try:
        print(                                           # Меню выбора операции
            "\n1. Addition"
            "\n2. Subtraction"
            "\n3. Multiplication"
            "\n4. Division"
        )
        choice = int(input("\nChoose operation: "))     # Пункт в меню всегда целое число
    except ValueError:
        print("Incorrect operation !")
        main_menu()
    else:
        if len(account_numbers) == empty_list:          # Если список чисел пустой, то считать нечего
            print("You not entered numbers.")
            entering()
        else:
            if choice == 1:
                print("Result is {}".format(addition(account_numbers)))
            elif choice == 2:
                print("Result is {}".format(subtraction(account_numbers)))
            elif choice == 3:
                print("Result is {}".format(multiplication(account_numbers)))
            elif choice == 4:
                print("Result is {}".format(division(account_numbers)))
            else:
                pass


if __name__ == "__main__":
    entering()
    main_menu()
